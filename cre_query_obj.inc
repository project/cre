<?php
/* cre_query_obj.inc
 Designed to create an query object for cre
*/

class _cre_query {
   // constructor
   function _cre_query($uid, $target_type,$reference_type = NULL) { // reference_type not used yet
      $this->average = $average;
      $this->columns = array();
      $this->tables = array();
      $this->conditions = array();
      $this->execute = false;
      
      $query_type = variable_get('cre_query_type', 'accurate');
      
      if ($query_type == 'accurate') {
         
         $this->columns[] = "d.content_id1 as 'content_id'";
         $this->columns[] = "sum(d.sum+d.count*r.value)/sum(d.count) as 'score'";
         
         $this->tables[] = "{cre_similarity_matrix} d";
         $this->tables[] = "{votingapi_vote} r";
         
         $this->conditions[] = sprintf("d.content_type1='%s'",$target_type);
         $this->conditions[] = sprintf("r.uid=%d",$uid);
         $this->conditions[] = "d.content_id1<>r.content_id";
         $this->conditions[] = "d.content_id2=r.content_id";
      }
      else if ($query_type == 'fast'){
          $db_average = db_query("SELECT sum/count as 'average' from {cre_average_vote}
                           WHERE uid = %d",$uid);
         $average_obj = db_fetch_object($db_average);
         $average= $average_obj->average;
         if (!$average) {
             return;
         }
         $this->columns[] = "d.content_id1 as 'content_id'";
         $this->columns[] = "sum(d.sum+d.count)/sum(d.count)+". $average . " as 'score'";
         
         $this->tables[] = "{cre_similarity_matrix} d";
         
         $this->conditions[] = sprintf("d.content_type1='%s'",$target_type);
      }
      else {
          /*
          *   point votingapi system query
          */
          $this->columns[] = "d.content_id1 as 'content_id'";
          $this->columns[] = "d.sum/d.count as 'score'";
          
          $this->tables[] = "{cre_similarity_matrix} d";
          $this->tables[] = "{votingapi_vote} r";
          
          $this->conditions[] = sprintf("d.content_type1='%s'",$target_type);
          $this->conditions[] = sprintf("r.uid = %d",$uid);
          $this->conditions[] = "r.value = 1";
          $this->conditions[] = "d.content_id2 = r.content_id";
          $this->conditions[] = "d.content_id1 <> d.content_id2";
      }
      $this->execute = true;
   }
   
   function add_column($column) {
      $this->columns[] = $column;
   }
   function add_table($proper_table_name) {
      $this->tables[] = $proper_table_name;
   }
   function add_where($new_condtion) {
      $this->conditions[] = $new_condtion;
   }
   function execute() {
      if(!$this->execute) {
         return false;
      }
   
      $query_string = "SELECT ";
      
      // columns
      $query_string = $query_string . implode(",",$this->columns);
      
      // tables
      $query_string = $query_string . " FROM " . implode(",",$this->tables);
      
      // conditions
      $query_string = $query_string . " WHERE " . implode(" AND ",$this->conditions);
      
      $query_string = $query_string. " GROUP BY d.content_id1 ORDER BY score DESC";
     
      return $query_string;
   }
}