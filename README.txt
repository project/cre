The Basics
==========
Content Recommendation Engine (CRE) recommends any voteable content via the VotingAPI [http://drupal.org/node/36041]. Because the votingapi does not allow for a user to actually place a vote, one of the modules listed below are also required:

SimpleVote
MediumVote
UserReview
Vote-Up-Down
LatestGreatest
LoveHate
Node Moderation

The above listed modules create voting 'widgets' and allow votes to be placed and stored. Once votes have been place, CRE is able to recommended the content via the various modules ( and other user contributed modules) included in Recommendation Modules folder that is included in this download.

For developers looking to design their own recommendation modules, see API.txt to understand the various function calls and the table structure.

Installation
============
First step is to copy ALL files included in this download into /modules directory of your website. This will let Drupal know that cre.module and the Recommendation modules exist. Then, goto http://www.example.com/admin/modules if you are using clean urls or http://www.example.com/?q=admin/modules if you are using non-clean urls and enable both the cre and the appropriate recommendation module ( for instance, if you are looking to recommend nodes to user, then enable node_recommendation module).

Installation of this module over existing voting data requires that cron jobs be enabled. This is because the installation will take much to long over a large existing votingapi data. The number of votes processed in a cron run can be adjusted by visting the url /admin/settings/cre but please be careful, 30 votes a run is a very safe setting and is enabled by default. A php runtime error could mess up the data structure and therefore deliver not so accurate results. 

Requirements
============
CRE requires Drupal 4.7 or later.

CRE requires VotingAPI module enable and an appropriate voting module (listed above).

Common Issues
================================
CRE will never recommend a piece of content if that user has voted already on that piece of content.

If there is only one user voting, then there will be no recommendations

If using a votingapi point system voting widget (see Vote-Up-Down, and other Digg like systems) remember to visit admin >> settings >> cre and tell cre that you are using it

CRE + Node_recommendation Common Issues
========================================
/recommendations has two behaviors
   1.) Display a list of up to max number of nodes (see admin >> settings >> node_recommendation)of recommended content
      OR
   2.) List the top rated nodes

   behavior number 2 occurs when:
     1.) User has not voted yet
     2.) User authored all nodes that have been voted on
     3.) User has voted on every node in votingapi_vote table
   otherwise, behavior number 1 occurs

General Node Recommendation block has ONLY one behavior and that is to display a list of recommended nodes. If any of the following conditions are true, then the block will not display:
     1.) User has not voted yet
     2.) User authored all nodes that have been voted on
     3.) User has voted on every node in votingapi_vote table

Similarly Rated Nodes block will display nodes (whether that user has voted on them or authored them) that have been voted on in a similar pattern as the currently viewed node. Essentially, those that liked this node also liked these nodes.

Recommend nodes of a particular type block functions identical to General Node Recommendation block BUT it will recommend nodes of a specific type (i.e. only recommend stories or pages or forum topics).
